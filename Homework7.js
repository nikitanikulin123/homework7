//Задание №1
var studentsAndPoints = [
'Алексей Петров', 0,
'Ирина Овчинникова', 60,
'Глеб Стукалов', 30,
'Антон Павлович', 30,
'Виктория Заровская', 30,
'Алексей Левенец', 70,
'Тимур Вамуш', 30,
'Евгений Прочан', 60,
'Александр Малов', 0
];
var students = studentsAndPoints.filter(function (student) {
    return typeof(student) === 'string';
});
var points = studentsAndPoints.filter(function (point) {
    return typeof(point) === 'number';
});
console.log(students, points);

////Задание №2
console.log('Список студентов:');
students.forEach(function (student, i) {
    console.log('Студент ' + student + ' набрал ' + points[i] + ' баллов');
});

//Задание №3
var pointIndex = 0;
var max = points.reduce(function(previous, current, index) {
    if(previous < current) {
        pointIndex = index;
        return current;
    }
    else
        return previous;

});
//var pointIndex = points.findIndex(function(x) {
//    return x === max;
//});
console.log('Студент набравший максимальный балл: ' + students[pointIndex] + ' (' + max + ' баллов)');

//Задание №4
students.map(function (student, i) {
    if(student === 'Ирина Овчинникова' || student === 'Александр Малов')
        return points[i] += 30;
});

//Дополнительное задание №5
var clonePoints = [];
var currentMax, currentPointIndex = 0;
var getTop = function(number) {
    var orderedStudentsAndPoints = [];

    points.forEach(function (point, i) {
        clonePoints[i] = point;
    });
    if(students.length < number)
        number = students.length;
    for (var i = 0; i < number; i++) {
        currentMax = clonePoints.reduce(function (previous, current, index) {
            if (previous < current) {
                currentPointIndex = index;
                return current;
            }
            else
                return previous;
        });
        //currentPointIndex = points.findIndex(function (x) {
        //    return x === currentMax;
        //});
        orderedStudentsAndPoints.push(students[currentPointIndex] + ' - ' + currentMax + ' баллов');
        delete(clonePoints[currentPointIndex]);
    }
    return orderedStudentsAndPoints;
};
console.log('Топ 3:');
console.log(getTop(3));
console.log('Топ 5:');
console.log(getTop(5));
